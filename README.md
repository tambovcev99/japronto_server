## Installing

Virtual environment:

1. Set up venv

```
python3 -m venv venv
source venv/bin/activate
```

2. Install japronto

```
pip install -r requirements.txt
```

<!-- ```
pip3 install https://github.com/squeaky-pl/japronto/archive/master.zip
``` -->

3. Start server

```
python3 main.py
```

4. Go to <a href="http://0.0.0.0:8080">http://0.0.0.0:8080</a>

---

or build & run in Docker

```
docker build -t japronto_app:1.0.0 .
docker run -d -p 8080:8080 japronto_app:1.0.0
```
