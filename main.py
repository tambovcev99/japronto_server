import io
import base64
from json import JSONDecodeError
from japronto.app import Application


def preparation(request):
    # text = """Body related properties:
    #     Mime type: {0.mime_type}
    #     Encoding: {0.encoding}
    #     Body: {0.body}
    #     Text: {0.text}
    #     Form parameters: {0.form}
    #     Files: {0.files}
    # """.format(request)
    try:
        json = request.json
        return request.Response(json=json)
    except JSONDecodeError:
        print("JSONDecodeError Error")
        return request.Response(code=500)


def synthesis(request):
    try:
        json = request.json
        # text = str(json)
        # text_bytes = str.encode(text)
        # print(text_bytes)
        data = open('sample.mp3', 'rb').read()
        return request.Response(body=data, mime_type="application/octet-stream")
    except RuntimeError:
        print("JSONDecodeError Error")
        return request.Response(code=500)


app = Application()
router = app.router
router.add_route('/preparation', preparation, method='POST')
router.add_route('/synthesis', synthesis, method='POST')
app.run()
